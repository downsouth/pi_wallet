# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# Configures the endpoint
config :pi_wallet, PiWallet.Web.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "bcURgLkYNGIvlLLnC2EBzPwPykW0kluxklF1bm3qnLbpqFD8Q6fRFjJIDd6xCZbv",
  render_errors: [view: PiWallet.Web.ErrorView, accepts: ~w(html json)],
  pubsub: [name: PiWallet.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"

config :phoenix,
  :filter_parameters, ["passphrase", "current_passphrase", "new_passphrase"]