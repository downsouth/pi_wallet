defmodule PiWallet.Mixfile do
  use Mix.Project

  def project do
    [app: :pi_wallet,
     version: "0.0.4",
     elixir: "~> 1.4",
     elixirc_paths: elixirc_paths(Mix.env),
     compilers: [:phoenix, :gettext] ++ Mix.compilers,
     start_permanent: Mix.env == :prod,
     deps: deps()]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [mod: {PiWallet.Application, []},
     extra_applications: [
      :logger, 
      :runtime_tools, 
      :edeliver, 
      :basic_auth,
      :httpoison, 
      :poison,
      :quantum
    ]]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_),     do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [{:phoenix, "~> 1.3.0-rc"},
     {:phoenix_pubsub, "~> 1.0"},
     {:phoenix_html, "~> 2.6"},
     {:phoenix_live_reload, "~> 1.0", only: :dev},
     {:gettext, "~> 0.11"},
     {:cowboy, "~> 1.0"},
     {:distillery, "~> 1.4", warn_missing: false},
     {:edeliver, "~> 1.4.0"},
     {:httpoison, "~> 0.13"},
     {:poison, "~> 3.1"},
     {:basic_auth, "~> 2.0"},
     {:quantum, ">= 2.1.0"},
     {:timex, "~> 3.0"}]
  end
end
