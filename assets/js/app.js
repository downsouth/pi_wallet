// Brunch automatically concatenates all files in your
// watched paths. Those paths can be configured at
// config.paths.watched in "brunch-config.js".
//
// However, those files will only be executed if
// explicitly imported. The only exception are files
// in vendor, which are never wrapped in imports and
// therefore are always executed.

// Import dependencies
//
// If you no longer want to use a dependency, remember
// to also remove its path from "config.paths.watched".
import "phoenix_html"

// Import local files
//
// Local files can be imported directly using relative
// paths "./socket" or full ones "web/static/js/socket".

import socket from "./socket"

$(() => {
  // Dashboard
  const $dashboardPageEl = $(".dashboard")
  const $addressEl = $(".dashboard table")
  const $addressListEl = $(".dashboard tbody")
  const $coinsEl = $("h2 span")
  const $btcEl = $(".btc span")
  const $usdEl = $(".usd span")
  const $eurEl = $(".eur span")
  const $gbpEl = $(".gbp span")
  const $stakingEl = $("h4")
  const $connectionsEl = $("h5 span")
  const $unlockFormEl = $("form.unlock")
  const $lockFormEl = $("form.lock")
  const $unencryptedAlertEl = $("#wallet_unencrypted_message")
  const $currencies = $(".currency");

  const roundNumber = (n) => {
    return Math.round(n * 100)/100;
  }

  const updateDashboard = (data) => {
    if(data.addresses && data.addresses.length > 0){
      $addressEl.show();
      $addressListEl.html("");
      data.addresses.forEach(function(a){
        $addressListEl.append("<tr><td>"+a+"</td></tr>");
      })
    } else {
      $addressEl.hide();
    }
    $coinsEl.text(data.coins)
    $btcEl.text(roundNumber(data.coins * data.btc_rate));
    $usdEl.text(roundNumber(data.coins * data.usd_rate));
    $eurEl.text(roundNumber(data.coins * data.eur_rate));
    $gbpEl.text(roundNumber(data.coins * data.gbp_rate));
    $stakingEl.text((data.staking ? "Staking" : "Not staking"));
    if(data.unlocked_until === null){
      $unencryptedAlertEl.removeClass("hidden");
    }else{
      $unencryptedAlertEl.addClass("hidden");
      if(data.unlocked_until > 0) {
        $lockFormEl.removeClass("hidden");
        $unlockFormEl.addClass("hidden");
      } else {
        $lockFormEl.addClass("hidden");
        $unlockFormEl.removeClass("hidden");
      }
    }
    $connectionsEl.text(data.connections);
  }

  if($currencies.length > 0){
    let currentCurrencyIndex = 0;
    let currentCurrencyMaxIndex = $currencies.length;
    if(currentCurrencyMaxIndex > 0){
      $currencies.on("click", (e) => {
        e.preventDefault();
        $currencies.addClass("hidden");
        currentCurrencyIndex += 1;
        if(currentCurrencyIndex > currentCurrencyMaxIndex - 1){
          currentCurrencyIndex = 0;
        }
        $($currencies[currentCurrencyIndex]).removeClass("hidden");
      });
    }

    $.ajax({
      url: "/api/info",
      dataType: "JSON",
      method: "GET",
      data: {}
    }).fail((e) => {
      console.log(e);
      alert("Failed to get dashboard data");
    }).done((data) => {
      updateDashboard(data);
    });
  }

  // Side Stakes
  const $newSideStakePanel = $("#new_stake_panel");
  const $newSideStakeBtn = $("#new_stake_btn");
  const $cancelSideStakeBtn = $("#cancel_stake_btn");
  const $stakesPageEl = $(".stakes")
  const $stakesListEl = $(".stakes tbody")

  const appendSideStake = (side_stake) => {
    $stakesListEl.append("<tr><td><i class='fas fa-code-branch fa-rotate-90'></i></td><td class='name'>"+side_stake.name+"</td><td class='address'>"+side_stake.address+"</td><td class='amount'>"+side_stake.percentage+"</td><td><a href='/commands/side_stakes/"+side_stake.address+"' class='btn btn-xs btn-primary' onclick=\"return confirm('Are you sure?');\">Delete</a></td></tr>")
  }

  const loadStakes = () => {
    $.ajax({
      url: "/api/side_stakes",
      dataType: "JSON",
      method: "GET",
      data: {}
    }).fail((e) => {
      console.log(e);
      alert("Failed to get side stakes");
    }).done((data) => {
      data.side_stakes.reverse().forEach(function(t){
        appendSideStake(t)
      });
    });
  }

  if($newSideStakeBtn.length > 0){
    $newSideStakeBtn.on("click", (e) =>{
      e.preventDefault();
      $newSideStakePanel.removeClass("hidden");
      $cancelSideStakeBtn.removeClass("hidden");
      $newSideStakeBtn.addClass("hidden");
    });
    $cancelSideStakeBtn.on("click", (e) =>{
      e.preventDefault();
      $newSideStakePanel.addClass("hidden");
      $cancelSideStakeBtn.addClass("hidden");
      $newSideStakeBtn.removeClass("hidden");
    });

    // Load side stakes
    loadStakes();
  }

  // Transactions
  const $newTransactionPanel = $("#new_transaction_panel");
  const $newTransactionBtn = $("#new_transaction_btn");
  const $cancelTransactionBtn = $("#cancel_transaction_btn");
  const $loadTransactionsBtn = $("#load_transactions_btn");
  const $transactionsPageEl = $(".transactions")
  const $transactionsListEl = $(".transactions tbody")

  let transactionsOffset = 0;
  let transactionsPageLimit = 20;
  const transactionClass = (category) => {
    switch(category){
      case "receive":
        return "fa-download"
        break
      case "send":
        return "fa-upload"
        break
      default:
        return "fa-cogs"
    }
  }

  const unixTimestampToTime = (timestamp) => {
    return new Date(timestamp*1000)
  }

  const transactionValue = (transaction) => {
    switch(transaction.category){
      case "receive":
        return transaction.amount
        break
      case "send":
        return (transaction.amount+transaction.fee)
        break
      default:
        return transaction.amount
    }
  }

  const appendTransaction = (transaction) => {
    $transactionsListEl.append("<tr id='"+transaction.txid+"' class='"+transaction.category+"' title='Transaction ID: "+transaction.txid+". Confirmations: "+transaction.confirmations+".'><td><i class='fas "+transactionClass(transaction.category)+"'></i></td><td class='time'>"+unixTimestampToTime(transaction.timereceived)+"</td><td class='address'>"+transaction.address+"</td><td class='amount'>"+transactionValue(transaction)+"</td></tr>")
  }

  const loadTransactions = (offset, limit) => {
    $loadTransactionsBtn.attr("disabled", true);

    $.ajax({
      url: "/api/transactions",
      dataType: "JSON",
      method: "GET",
      data: {offset: offset, limit: limit}
    }).fail((e) => {
      console.log(e);
      alert("Failed to get transactions");
    }).done((data) => {
      data.transactions.reverse().forEach(function(t){
        appendTransaction(t)
      });
    }).always(() => {
      $loadTransactionsBtn.removeAttr("disabled");
    });
  }

  if($newTransactionBtn.length > 0){
    $newTransactionBtn.on("click", (e) =>{
      e.preventDefault();
      $newTransactionPanel.removeClass("hidden");
      $cancelTransactionBtn.removeClass("hidden");
      $newTransactionBtn.addClass("hidden");
    });
    $cancelTransactionBtn.on("click", (e) =>{
      e.preventDefault();
      $newTransactionPanel.addClass("hidden");
      $cancelTransactionBtn.addClass("hidden");
      $newTransactionBtn.removeClass("hidden");
    });

    $loadTransactionsBtn.on("click", (e) => {
      e.preventDefault();
      // TODO: Should load the transactions into prepared containers tagged with the offset so they don't end up in the wrong order from mixed async responses
      loadTransactions(transactionsOffset, transactionsPageLimit);
      transactionsOffset += transactionsPageLimit;
    })
    // Load initial transactions
    loadTransactions(transactionsOffset, transactionsPageLimit);
    transactionsOffset += transactionsPageLimit;
  }

  // Settings
  const $ssid_input = $("#command_ssid");
  const $ssidList = $("#ssid_list");
  const $ssidRefreshBtn = $("#ssid_list_refresh");
  if($ssidList.length > 0) {
    let $firstOption = $($ssidList.children()[0]);
    const refreshSSIDList = () => {
      let loaded = false;
      let dots = 0;
      let text = "Searching for networks";
      let timer = null;
      let $loadingOption = $firstOption.clone();
      $ssidList.html("");
      $ssidList.append($loadingOption);
      let updateLoadingText = () => {
        dots += 1;
        if(dots > 3){
          dots = 1;
        }
        let loadingText = text;
        for(let i = dots;i > 0;i--){
          loadingText += ".";
        }
        $loadingOption.text(loadingText);
        timer = setTimeout(updateLoadingText, 500);
      }
      updateLoadingText();

      $.ajax({
        url: "/api/wifi_networks",
        dataType: "JSON",
        method: "GET",
        data: {}
      }).fail((e) => {
        console.log(e);
        alert("Failed to get networks");
      }).done((data) => {
        $ssidList.html("");
        $ssidList.append($("<option value=''>Select Network</option>"));
        data.networks.forEach((network) => {
          $ssidList.append($("<option value='"+network.ssid+"'>"+network.ssid+"</option>"));
        });
        $ssidList.removeAttr("disabled");
      }).always(() => {
          clearTimeout(timer);
      });
    }
    $ssidRefreshBtn.on("click", (e) => { e.preventDefault(); refreshSSIDList(); });
    $ssidList.on("change", (e) => { let $selectedOption = $ssidList.find(":selected"); $ssid_input.val($selectedOption.val()); });
    refreshSSIDList();
  }
});