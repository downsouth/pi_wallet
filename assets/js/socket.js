// NOTE: The contents of this file will only be executed if
// you uncomment its entry in "assets/js/app.js".

// To use Phoenix channels, the first step is to import Socket
// and connect at the socket path in "lib/web/endpoint.ex":
import {Socket} from "phoenix"

let socket = new Socket("/socket", {params: {token: window.userToken}})

// When you connect, you'll often need to authenticate the client.
// For example, imagine you have an authentication plug, `MyAuth`,
// which authenticates the session and assigns a `:current_user`.
// If the current user exists you can assign the user's token in
// the connection for use in the layout.
//
// In your "lib/web/router.ex":
//
//     pipeline :browser do
//       ...
//       plug MyAuth
//       plug :put_user_token
//     end
//
//     defp put_user_token(conn, _) do
//       if current_user = conn.assigns[:current_user] do
//         token = Phoenix.Token.sign(conn, "user socket", current_user.id)
//         assign(conn, :user_token, token)
//       else
//         conn
//       end
//     end
//
// Now you need to pass this token to JavaScript. You can do so
// inside a script tag in "lib/web/templates/layout/app.html.eex":
//
//     <script>window.userToken = "<%= assigns[:user_token] %>";</script>
//
// You will need to verify the user token in the "connect/2" function
// in "lib/web/channels/user_socket.ex":
//
//     def connect(%{"token" => token}, socket) do
//       # max_age: 1209600 is equivalent to two weeks in seconds
//       case Phoenix.Token.verify(socket, "user socket", token, max_age: 1209600) do
//         {:ok, user_id} ->
//           {:ok, assign(socket, :user, user_id)}
//         {:error, reason} ->
//           :error
//       end
//     end
//
// Finally, pass the token on connect as below. Or remove it
// from connect if you don't care about authentication.

socket.connect()

// Now that you are connected, you can join channels with a topic:
let channel = socket.channel("values", {})
let dashboardPageEl = $(".dashboard")
let addressEl = $(".dashboard table")
let addressListEl = $(".dashboard tbody")
let coinsEl = $("h2 span")
let btcEl = $(".btc span")
let usdEl = $(".usd span")
let eurEl = $(".eur span")
let gbpEl = $(".gbp span")
let stakingEl = $("h4")
let connectionsEl = $("h5 span")
let unlockFormEl = $("form.unlock")
let lockFormEl = $("form.lock")

function roundNumber(n) {
  return Math.round(n * 100)/100
}

channel.on('update', payload => {
  if(dashboardPageEl.length == 0){
    return
  }
  if(payload.addresses && payload.addresses.length > 0){
    addressEl.show();
    addressListEl.html("")
    payload.addresses.forEach(function(a){
      addressListEl.append("<tr><td>"+a+"</td></tr>")
    })
  } else {
    addressEl.hide();
  }
  coinsEl.text(payload.coins)
  btcEl.text(roundNumber(payload.coins * payload.btc_rate))
  usdEl.text(roundNumber(payload.coins * payload.usd_rate))
  eurEl.text(roundNumber(payload.coins * payload.eur_rate))
  gbpEl.text(roundNumber(payload.coins * payload.gbp_rate))
  stakingEl.text((payload.staking ? "Staking" : "Not staking"))
  if(payload.unlocked_until > 0) {
    lockFormEl.removeClass("hidden")
    unlockFormEl.addClass("hidden")
  } else {
    lockFormEl.addClass("hidden")
    unlockFormEl.removeClass("hidden")
  }
  connectionsEl.text(payload.connections)
})

channel.join()
  .receive("ok", resp => { console.log("Joined successfully", resp) })
  .receive("error", resp => { console.log("Unable to join", resp) })


// Transactions_
// function transactionClass(category){
//   switch(category){
//     case "receive":
//       return "fa-download"
//       break
//     case "send":
//       return "fa-upload"
//       break
//     default:
//       return "fa-cogs"
//   }
// }

// function unixTimestampToTime(timestamp){
//   return new Date(timestamp*1000)
// }

// function transactionValue(transaction){
//   switch(transaction.category){
//     case "receive":
//       return transaction.amount
//       break
//     case "send":
//       return (transaction.amount+transaction.fee)
//       break
//     default:
//       return transaction.amount
//   }
// }

// let transactions_channel = socket.channel("transactions", {})
// let transactionsPageEl = $(".transactions")
// let transactionsListEl = $(".transactions tbody")

// transactions_channel.on('update', payload => {
//   if(transactionsPageEl.length == 0){
//     return
//   }
//   if(payload.transactions && payload.transactions.length > 0){
//     transactionsListEl.html("")
//     payload.transactions.reverse().forEach(function(a){
//       transactionsListEl.append("<tr id='"+a.txid+"' class='"+a.category+"' title='Transaction ID: "+a.txid+". Confirmations: "+a.confirmations+".'><td><i class='fas "+transactionClass(a.category)+"'></i></td><td class='time'>"+unixTimestampToTime(a.timereceived)+"</td><td class='address'>"+a.address+"</td><td class='amount'>"+transactionValue(a)+"</td></tr>")
//     })
//   }
// })

// transactions_channel.join()
//   .receive("ok", resp => { console.log("Joined successfully", resp) })
//   .receive("error", resp => { console.log("Unable to join", resp) })

export default socket
