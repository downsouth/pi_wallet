### Pink Daemon RPC Reference

addmultisigaddress <nrequired> <'["key","key"]'> [account]
addredeemscript <redeemScript> [account]
addstakeout <name> <address> <percentage>
backupwallet <destination>
checkwallet
clearwallettransactions 
createrawtransaction [{"txid":txid,"vout":n},...] {address:amount,...}
decoderawtransaction <hex string>
decodescript <hex string>
delstakeout <address>
dumpprivkey <pinkcoinaddress>
dumpwallet <filename>
getaccount <pinkcoinaddress>
getaccountaddress <account>
getaddressesbyaccount <account>
getbalance [account] [minconf=1]
getbestblockhash
getblock <hash> [txinfo]
getblockbynumber <number> [txinfo]
getblockcount
getblockhash <index>
getblocktemplate [params]
getcheckpoint
getconnectioncount
getdifficulty
getinfo
getmininginfo
getnewaddress [account]
getnewpubkey [account]
getnewstealthaddress [label]
getnodes
getpeerinfo
getrawmempool
getrawtransaction <txid> [verbose=0]
getreceivedbyaccount <account> [minconf=1]
getreceivedbyaddress <pinkcoinaddress> [minconf=1]
getstakeoutinfo
getstakesplitthreshold
getstakinginfo
getsubsidy [nTarget]
gettransaction <txid>

getwalletinfo

    Returns an object containing various wallet state info.

    Result:
    {
    "walletversion": xxxxx, (numeric) the wallet version
    "balance": xxxxxxx, (numeric) the total pinkcoin balance of the wallet
    "txcount": xxxxxxx, (numeric) the total number of transactions in the wallet
    "keypoololdest": xxxxxx, (numeric) the timestamp (seconds since GMT epoch) of the oldest pre-generated key in the key pool
    "keypoolsize": xxxx, (numeric) how many new keys are pre-generated
    "unlocked_until": ttt, (numeric) the timestamp in seconds since epoch (midnight Jan 1 1970 GMT) that the wallet is unlocked for transfers, or 0 if the wallet is locked
    }

getwork [data]
getworkex [data, coinbase]
help [command]
importprivkey <pinkcoinprivkey> [label]
importstealthaddress <scan_secret> <spend_secret> [label]
importwallet <filename>

    Imports keys from a wallet dump file (see dumpwallet).

keypoolrefill [new-size]
listaccounts [minconf=1]

    Returns Object that has account names as keys, account balances as values.

listaddressgroupings

    Lists groups of addresses which have had their common ownership
    made public by common use as inputs or as the resulting change
    in past transactions

listreceivedbyaccount [minconf=1] [includeempty=false]
listreceivedbyaddress [minconf=1] [includeempty=false]
listsinceblock [blockhash] [target-confirmations]
liststakeout

    Returns the current Stakeout entries in stake database.

liststealthaddresses [show_secrets=0]
listtransactions [account] [count=10] [from=0]

    Returns up to [count] most recent transactions skipping the first [from] transactions for account [account].
    
listunspent [minconf=1] [maxconf=9999999]  ["address",...]
makekeypair [prefix]
move <fromaccount> <toaccount> <amount> [minconf=1] [comment]
repairwallet
resendtx
reservebalance [<reserve> [amount]]
scanforalltxns [fromHeight]
scanforstealthtxns [fromHeight]
sendalert <message> <privatekey> <minver> <maxver> <priority> <id> [cancelupto]
sendfrom <fromaccount> <topinkcoinaddress> <amount> [minconf=1] [note] [comment] [comment-to]
sendmany <fromaccount> {address:amount,...} [minconf=1] [comment]
sendrawtransaction <hex string>

sendtoaddress <pinkcoinaddress> <amount> [comment] [comment-to] [note]

    <amount> is a real and is rounded to the nearest 0.000001

    requires wallet passphrase to be set with walletpassphrase first
sendtostealthaddress <stealth_address> <amount> [comment] [comment-to] [note]
setaccount <pinkcoinaddress> <account>

    Sets the account associated with the given address.

setstakesplitthreshold <1 - 999,999>
settxfee <amount>
signmessage <pinkcoinaddress> <message>
signrawtransaction <hex string> [{"txid":txid,"vout":n,"scriptPubKey":hex,"redeemScript":hex},...] [<privatekey1>,...] [sighashtype="ALL"]
smsgaddkey <address> <pubkey>
smsgbuckets [stats|dump]
smsgdisable 
smsgenable 
smsggetpubkey <address>
smsginbox [all|unread|clear]
smsglocalkeys [whitelist|all|wallet|recv <+/-> <address>|anon <+/-> <address>]
smsgoptions [list|set <optname> <value>]
smsgoutbox [all|clear]
smsgscanbuckets 
smsgscanchain 
smsgsend <addrFrom> <addrTo> <message>
smsgsendanon <addrTo> <message>
stop <detach>
submitblock <hex data> [optional-params-obj]
validateaddress <pinkcoinaddress>
validatepubkey <pinkcoinpubkey>
verifymessage <pinkcoinaddress> <signature> <message>
walletlock

    Removes the wallet encryption key from memory, locking the wallet.
    After calling this method, you will need to call walletpassphrase again
    before being able to call any methods which require the wallet to be unlocked.

walletpassphrase <passphrase> <timeout> [stakingonly]

    Stores the wallet decryption key in memory for <timeout> seconds.
    if [stakingonly] is true sending functions are disabled.

walletpassphrasechange <oldpassphrase> <newpassphrase>