defmodule PiWallet.Web.PageControllerTest do
  use PiWallet.Web.ConnCase

  describe "unauthorized" do
    test "no auth", %{conn: conn} do
      conn = get conn, "/"
      assert response(conn, 401)
    end

    test "incorrect auth", %{conn: conn} do
      header_content = "Basic " <> Base.encode64("pink:incorrect_password")
      conn = conn
      |> put_req_header("authorization", header_content)
      |> get("/")
      assert response(conn, 401)
    end
  end

  describe "authorized" do  
    test "GET /", %{conn: conn} do
      header_content = "Basic " <> Base.encode64("pink:test")
      conn = conn
      |> put_req_header("authorization", header_content)
      |> get("/")
      assert response(conn, 200)
    end
  end
end
