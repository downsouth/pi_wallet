# Pink Pi Web Interface

### What is it
A very experimental web interface for controlling a Raspberry Pi running a Pink Coin daemon.

### Authors
Developed by Josh Algdal & Marcus Bergstr�m in collaboration with Jean Marc Gimenez and the Pink Coin Team.

### Requirements
Raspberry Pi with Pink Coin daemon configured to run with RPC interface. It has been tested on Raspberry Pi 2, zero w and 3. The Raspberry Pi 3 is currently the recommended device due to the multi core CPU architecture and amount of RAM memory.

### Development
Set the RPC credentials in the /pinkconf.txt file, on macOS this is located in `~/Library/Application Support/pink2/`

Start the Pink Coin daemon either via a standalone unix deamon or via the wallet app, e.g. on macOS ` /Applications/Pinkcoin-Qt.app/Contents/MacOS/Pinkcoin-Qt -server`.

Start your App server:

  * Install dependencies with `mix deps.get`
  * Install Node.js dependencies with `cd assets && npm install`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

### Caution
Please note that this is an experimental implementation and far from production ready. Use with caution.