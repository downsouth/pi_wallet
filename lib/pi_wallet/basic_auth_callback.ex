defmodule PiWallet.BasicAuthCallback do
  require Logger
  @moduledoc """
  Basic auth plugin functions that retrieve credentials from application config.
  """

  alias Plug.Crypto

  def check_credentials(conn, username, password) do
    if Crypto.secure_compare(configured_token({:pi_wallet, :basic_auth}), username <> ":" <> password) do
      conn
    else
      Logger.error "Authentication failed: '#{username}' - #{to_string(:inet_parse.ntoa(conn.remote_ip))}"
      Plug.Conn.halt(conn)
    end
  end

  defp to_value({:system, env_var}), do: System.get_env(env_var)
  defp to_value(value), do: value

  defp configured_token(config_options) do
    username(config_options) <> ":" <> password(config_options)
  end

  defp username(config_options), do: credential_part!(config_options, :username)

  defp password(config_options), do: credential_part!(config_options, :password)

  defp credential_part({app, key}, part) do
    app
    |> Application.fetch_env!(key)
    |> Keyword.get(part)
    |> to_value()
  end

  defp credential_part!(config_options, part) do
    case credential_part(config_options, part) do
      nil -> raise(ArgumentError, "Missing #{inspect(part)} from #{inspect(config_options)}")
      value -> value
    end
  end
end