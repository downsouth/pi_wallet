defmodule PiWallet.WalletClient do

  def stop_server() do
    call("stop")
  end

  def unlock(passphrase, staking_only \\ true) do
    call("walletpassphrase", [passphrase, 31536000, staking_only])
  end

  def lock() do
    call("walletlock")
  end

  def balance() do
    call("getbalance")
  end

  def wallet_info() do
    call("getwalletinfo")
  end

  def staking_info() do
    call("getstakinginfo")
  end

  def connection_count() do
    call("getconnectioncount")
  end

  def addresses() do
    call("getaddressesbyaccount", [""])
  end

  def transactions(account \\ "", count \\ 20, from \\ 0) do
    call("listtransactions", [account, count, from])
  end

  def create_transaction(address, amount, comment \\ nil, comment_to \\ nil, note \\ nil) do
    call("sendtoaddress", [address, amount, comment, comment_to, note])
  end

  def info() do
    call("getinfo")
  end

  def dump_wallet(filename) do
    call("dumpwallet", [filename])
  end

  def backup_wallet(filename) do
    call("backupwallet", [filename])
  end

  def import_wallet(filename) do
    call("importwallet", [filename])
  end

  def check_wallet() do
    call("checkwallet")
  end

  def clear_wallet_transactions() do
    call("clearwallettransactions")
  end

  def scan_for_all_transactions(height \\ 0) do
    call("scanforalltxns", [height])
  end 

  def scan_for_stealth_transactions(height \\ 0) do
    call("scanforstealthtxns", [height])
  end 

  def encrypt_wallet(new_passphrase) do
    call("encryptwallet", [new_passphrase])
  end

  def change_wallet_passphrase(old_passphrase, new_passphrase) do
    call("walletpassphrasechange", [old_passphrase, new_passphrase])
  end

  def list_stakeout() do
    call("liststakeout")
  end

  def add_stakeout(name, address, percentage) do
    call("addstakeout", [name, address, percentage])
  end

  def del_stakeout(address) do
    call("delstakeout", [address])
  end

  defp call(command, params \\ []) do
    with url <- Application.get_env(:pi_wallet, :wallet_url),
      command <- %{jsonrpc: "1.0", method: command, params: params},
      {:ok, body} <- Poison.encode(command),
      {:ok, response} <- HTTPoison.post(url, body),
      {:ok, metadata} <- Poison.decode(response.body),
      %{"error" => nil, "result" => result} <- metadata do
        {:ok, result}
    else
      %{"error" => reason} -> {:error, reason}
      error -> error
    end
  end

end
