defmodule PiWallet.WifiScanner do
  def networks do
    networks_info()
    |> extract_network_fragments()
    |> Enum.map(&extract_network_details/1)
  end

  defp extract_network_details(fragment) do
    {extract_ssid(fragment), "unknown"}
  end

  defp extract_ssid(fragment) do
    Regex.run(~r/SSID: (.*)\n/, fragment)
    |> Enum.at(1)
  end

  defp extract_network_fragments(networks_string) do
    String.split(networks_string, ~r{\nBSS })
  end

  defp networks_info do
    if System.get_env("MIX_ENV") == "prod" do
      {networks, 0} = System.cmd("iw", ~w(dev wlan0 scan))
      networks
    else
      File.read!("networks.out")
    end
  end
end
