defmodule PiWallet.WifiConfig do
  def read() do
    content = File.read!(wifi_settings_filename())
    {ssid, psk} = extract_config_details(content)
    %{"ssid" => ssid, "psk" => psk}
  end

  def write(ssid, psk) do
    temp_filename = "#{temp_path()}wpa_supplicant.conf.tmp"
    real_filename = wifi_settings_filename()
    write_temp_config_file(temp_filename, ssid, psk)
    move_temp_file(temp_filename, real_filename)
  end

  def switch_to_client_mode() do
    # Comment out all lines in /etc/network/interfaces.d/wlan0
    if File.exists?(wlan_interface_settings_filename()) do
      comment_out_file(wlan_interface_settings_filename())
    end
    # Comment out all lines in /etc/hostapd/hostapd.conf
    if File.exists?(hostapd_settings_filename()) do
      comment_out_file(hostapd_settings_filename())
    end
    # Comment out the last line in /etc/dhcpcd.conf
    if File.exists?(dhcp_settings_filename()) do
      deny_interface_index = File.read!(dhcp_settings_filename())
      |> String.split(~r{\n})
      |> Enum.find_index(fn(line) -> Regex.match?(~r/^denyinterfaces/, line) end)
      if deny_interface_index != nil do
        comment_out_file(dhcp_settings_filename(), deny_interface_index)
      end
    end
    # Either restart networking and turn off hostapd or reboot
    # Only reboot in production
    if System.get_env("MIX_ENV") == "prod" do
      execute_shell_command("reboot")
    end
  end

  defp write_temp_config_file(temp_filename, ssid, psk) do

    File.write!(temp_filename, """
country=GB
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
network={
        ssid="#{ssid}"
        scan_ssid=1
        psk="#{psk}"
}
    """, [:write])
  end

  defp move_temp_file(temp_filename, real_filename) do
    execute_shell_command("mv", [temp_filename, real_filename])
  end

  defp extract_config_details(config) do
    {extract_ssid(config), extract_psk(config)}
  end

  defp extract_ssid(config) do
    case Regex.run(~r/ssid="(.*)"\n/, config) do
      [_, val] ->
        val
      _ ->
        nil
    end
  end

  defp extract_psk(config) do
    case Regex.run(~r/psk="(.*)"\n/, config) do
      [_, val] ->
        val
      _ ->
        nil
    end
  end

  def backup_exists?() do
    File.exists?(backup_path())
  end

  def backup_settings() do
    execute_shell_command("mkdir", [backup_path()])
    Enum.each([
      wifi_settings_filename(),
      wlan_interface_settings_filename(),
      hostapd_settings_filename(),
      dhcp_settings_filename()
    ], fn filename ->
      execute_shell_command("cp", [filename, backup_path()])
    end)
  end

  defp backup_path() do
    if System.get_env("MIX_ENV") == "prod" do
      "/home/pi/settings_backup/"
    else
      "dev_settings_backup/"
    end
  end

  defp temp_path() do
    if System.get_env("MIX_ENV") == "prod" do
      "/home/pi/tmp/"
    else
      "tmp/"
    end
  end

  defp wifi_settings_filename() do
    if System.get_env("MIX_ENV") == "prod" do
      "/etc/wpa_supplicant/wpa_supplicant.conf"
    else
      "dev_device_config/wpa_supplicant.conf"
    end
  end

  defp dhcp_settings_filename() do
    if System.get_env("MIX_ENV") == "prod" do
      "/etc/dhcpcd.conf"
    else
      "dev_device_config/dhcpcd.conf"
    end
  end

  defp hostapd_settings_filename() do
    if System.get_env("MIX_ENV") == "prod" do
      "/etc/hostapd/hostapd.conf"
    else
      "dev_device_config/hostapd.conf"
    end
  end

  defp wlan_interface_settings_filename() do
    if System.get_env("MIX_ENV") == "prod" do
      "/etc/network/interfaces.d/wlan0"
    else
      "dev_device_config/wlan0"
    end
  end

  defp execute_shell_command(cmd, params \\ []) do
    if System.get_env("MIX_ENV") == "prod" do
      System.cmd("sudo", Enum.concat([cmd], params))
    else
      System.cmd("#{cmd}", params)
    end
  end

  defp comment_out_file(filename, index \\ nil) do
    temp_filename = "#{temp_path()}tmpfile.tmp"
    real_filename = filename
    execute_shell_command("cp", [real_filename, temp_filename])
    execute_shell_command("chmod", ["og+w", temp_filename])
    file_content = File.read!(temp_filename)
    commented_content = case index do 
      nil ->
        comment_out_lines(file_content)
      _ ->
        comment_out_lines(file_content, index)
    end
    File.write!(temp_filename, commented_content, [:write])
    execute_shell_command("chmod", ["og-w", temp_filename])
    execute_shell_command("mv", [temp_filename, real_filename])
  end

  defp comment_out_lines(content, index) do
    lines = String.split(content, ~r{\n})
    line = Enum.at(lines, index)
    unless Regex.match?(~r/^#/, line) do
      List.replace_at(lines, index, "# #{line}")
      |> Enum.join("\n")
    else
      content
    end
  end

  defp comment_out_lines(content) do
    # Use Regex.replace instead?
    String.split(content, ~r{\n})
    |> Enum.map(fn line -> 
      unless Regex.match?(~r/^#/, line) do
        "# #{line}"
      else
        line
      end
    end)
    |> Enum.join("\n")
  end
end
