defmodule PiWallet.Updater do
  alias PiWallet.CoinStatsClient
  alias PiWallet.WalletClient
  
  def send_update do
    # try do
      {:ok, info} = WalletClient.info()
      {:ok, staking} = WalletClient.staking_info()
      {:ok, addresses} = WalletClient.addresses()
      # Rates pulls the EUR value but the return also includes the USD and BTC prices
      rates = CoinStatsClient.rates
      gbp_rates = CoinStatsClient.gbp_rate
      {btc_rate, _} = Float.parse(Enum.at(rates, 0)["price_btc"])
      {usd_rate, _} = Float.parse(Enum.at(rates, 0)["price_usd"])
      {eur_rate, _} = Float.parse(Enum.at(rates, 0)["price_eur"])
      {gbp_rate, _} = Float.parse(Enum.at(gbp_rates, 0)["price_gbp"])
      PiWallet.Web.Endpoint.broadcast! "values", "update", %{coins: Float.round(info["balance"], 2), btc_rate: btc_rate, usd_rate: usd_rate, eur_rate: eur_rate, gbp_rate: gbp_rate, staking: staking["staking"], connections: info["connections"], addresses: addresses, unlocked_until: info["unlocked_until"], server_version: info["version"]}
    # rescue
    #   e in MatchError -> e
    # end
  end

  def send_transactions_update do
    # try do
      {:ok, transactions} = WalletClient.transactions()
      IO.inspect transactions
      PiWallet.Web.Endpoint.broadcast! "transactions", "update", %{transactions: transactions}
    # rescue
    #   e in MatchError -> e
    # end
  end
end