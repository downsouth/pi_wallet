defmodule PiWallet.CoinStatsClient do

  def rates do
    # When querying for euro the response includes btc and usd
    rate(:eur)
  end

  def btc_rate do
    rate(:btc)
  end

  def usd_rate do
    rate(:usd)
  end

  def eur_rate do
    rate(:eur)
  end

  def gbp_rate do
    rate(:gbp)
  end

  defp rate(currency) do
    url_suffix = case currency do
      :btc ->
        'BTC'
      :eur ->
        'EUR'
      :gbp ->
        'GBP'
      _ ->
        'USD'
    end
    
    with url <- "https://api.coinmarketcap.com/v1/ticker/pinkcoin/?convert=#{url_suffix}",
      {:ok, response} <- HTTPoison.get(url),
      {:ok, data} <- Poison.decode(response.body),
      %{"error" => nil, "result" => result} <- data do
        {:ok, result}
    else
      %{"error" => reason} -> {:error, reason}
      error -> error
    end

  end
end