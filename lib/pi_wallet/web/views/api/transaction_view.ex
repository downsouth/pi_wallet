defmodule PiWallet.Web.Api.TransactionView do
  use PiWallet.Web, :view

  def render("index.json", %{transactions: transactions, offset: offset, limit: limit}) do
    %{
      transactions: transactions,
      offset: offset,
      limit: limit
    }
  end

end
