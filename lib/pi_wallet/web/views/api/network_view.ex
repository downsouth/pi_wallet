defmodule PiWallet.Web.Api.NetworkView do
  def render("index.json", %{networks: networks}) do
    %{
      networks: Enum.map(networks, &network_json/1)
    }
  end

  def network_json(network) do
    {ssid, cipher} = network
    %{
      ssid: ssid,
      cipher: cipher
    }
  end
end