defmodule PiWallet.Web.Api.InfoView do
  def render("index.json", %{coins: coins, btc_rate: btc_rate, usd_rate: usd_rate, eur_rate: eur_rate, gbp_rate: gbp_rate, staking: staking, connections: connections, addresses: addresses, unlocked_until: unlocked_until, server_version: server_version}) do
    %{
      coins: coins, 
      btc_rate: btc_rate, 
      usd_rate: usd_rate, 
      eur_rate: eur_rate, 
      gbp_rate: gbp_rate, 
      staking: staking, 
      connections: connections, 
      addresses: addresses, 
      unlocked_until: unlocked_until, 
      server_version: server_version
    }
  end
end