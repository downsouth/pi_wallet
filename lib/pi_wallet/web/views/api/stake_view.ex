defmodule PiWallet.Web.Api.StakeView do
  use PiWallet.Web, :view

  def render("index.json", %{side_stakes: side_stakes}) do
    %{
      side_stakes: Enum.map(side_stakes, fn(side_stake) -> %{name: side_stake["Name: "], percentage: side_stake["Percentage: "], address: side_stake["Address: "]} end)
    }
  end

end
