defmodule PiWallet.Web.LayoutView do
  use PiWallet.Web, :view

  def render_shared(template, assigns \\ []) do
    render(PiWallet.Web.LayoutView, template, assigns)
  end
end
