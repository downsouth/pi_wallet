defmodule PiWallet.Web.SettingController do
  use PiWallet.Web, :controller
  alias PiWallet.WifiConfig
  alias PiWallet.WalletClient

  def index(conn, _params) do
    render conn, "index.html", wifi_config: WifiConfig.read(), wallet_encrypted: wallet_is_encrypted()
  end

  defp wallet_is_encrypted() do
    case WalletClient.wallet_info() do
      {:ok, %{"unlocked_until" => _}} ->
        true
      _ ->
        false
    end
  end
end

