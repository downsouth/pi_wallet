defmodule PiWallet.Web.StakeController do
  use PiWallet.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
