defmodule PiWallet.Web.CommandController do
  use PiWallet.Web, :controller
  alias PiWallet.WalletClient
  alias PiWallet.WifiConfig

  def lock(conn, _params) do
    WalletClient.lock
    redirect_to_root(conn)
  end

  def unlock(conn, %{"command" => command}) do
    %{"passphrase" => passphrase} = command
    WalletClient.unlock(passphrase)
    redirect_to_root(conn)
  end

  def set_wallet_passphrase(conn, %{"command" => command}) do
    wallet_encrypted = wallet_is_encrypted()
    if wallet_encrypted do
      %{"current_passphrase" => current_passphrase, "new_passphrase" => new_passphrase, "new_passphrase_confirmation" => new_passphrase_confirmation} = command
      if new_passphrase == new_passphrase_confirmation do
        # The wallet is already encrypted, let's change the passphrase
        WalletClient.change_wallet_passphrase(current_passphrase, new_passphrase)
      end
    else
      %{"new_passphrase" => new_passphrase, "new_passphrase_confirmation" => new_passphrase_confirmation} = command
      if new_passphrase == new_passphrase_confirmation do
        # The wallet is not encrypted, encrypt it
        WalletClient.encrypt_wallet(new_passphrase)
      end
    end
    redirect_to_settings(conn)
  end

  # def upload_interface(conn, %{"command" => command}) do
    # TODO: Pending
  # end

  # def upload_daemon(conn, %{"command" => command}) do
    # TODO: Pending
  # end

  def upload_wallet(conn, %{"command" => command}) do
    file = command["file"]
    filename = Application.get_env(:pi_wallet, :wallet_file_path)
    # Stop the wallet server. The server will automatically restart. Seems to be the only way of getting it to read the new file.
    WalletClient.stop_server
    # Overwrite the existing wallet file with the newly uploaded one
    File.cp! file.path, filename
    redirect_to_settings(conn)
  end

  def download_wallet(conn, _) do
    filename = Application.get_env(:pi_wallet, :tmp_file_path)
    # Backup the wallet to a temporary file
    WalletClient.backup_wallet(filename)
    # Send the file to the browser
    send_attachment(conn, {:file, filename}, %{filename: 'wallet.dat'})
    # Delete the temproary file
    File.rm(filename)
  end

  def update_wifi_settings(conn, %{"command" => command}) do
    %{"ssid" => ssid, "passphrase" => passphrase} = command
    # Backup settings the first time
    unless WifiConfig.backup_exists?() do
      WifiConfig.backup_settings()
    end
    WifiConfig.write(ssid, passphrase)
    if System.get_env("MIX_ENV") == "prod" do
      WalletClient.stop_server()
    end
    WifiConfig.switch_to_client_mode()
    redirect_to_settings(conn)
  end

  # def create_transaction(address, amount, comment \\ nil, comment_to \\ nil, note \\ nil) do
  #   call("sendtoaddress", [address, amount, comment, comment_to, note])
  # end

  def create_transaction(conn, %{"command" => command}) do
    %{
      "passphrase" => passphrase, 
      "address" => address, 
      "amount" => amount,
      "note" => note
    } = command
    {:ok, wallet_info} = WalletClient.wallet_info()
    # Check if the wallet is encrypted
    wallet_encrypted = wallet_info["unlocked_until"] != nil
    if wallet_encrypted do
      # Lock the wallet so we can unlock it for transactions
      WalletClient.lock()
      # Unlock it to make the transaction
      WalletClient.unlock(passphrase, false)
    end
    # Convert the amount to float
    {float_amount, _} = Float.parse(amount)
    # Create the transaction
    WalletClient.create_transaction(address, float_amount, nil, nil, note)
    # Return the wallet to initial locking state if the wallet is encrypted
    if wallet_encrypted do
      unless wallet_info["unlocked_until"] == 0 do
        # Unlock the wallet for staking again if it was unlocked
        WalletClient.unlock(passphrase, true)
      else
        # Lock the wallet again if it was locked
        WalletClient.lock()
      end
    end
    redirect_to_transactions(conn)
  end

  def clear_transactions(conn, _) do
    WalletClient.clear_wallet_transactions()
    redirect_to_settings(conn)
  end

  def scan_for_transactions(conn, _) do
    WalletClient.scan_for_all_transactions()
    redirect_to_settings(conn)
  end

  def add_side_stake(conn, %{"command" => command}) do
    %{
      "name" => name,
      "address" => address, 
      "percentage" => percentage
    } = command
    {float_percentage, _} = Float.parse(percentage)
    WalletClient.add_stakeout(name, address, Float.to_string(float_percentage))
    redirect_to_page(conn, :stakes)
  end

  # def remove_side_stake(conn, %{"command" => command}) do
  #   %{
  #     "address" => address
  #   } = command
  def remove_side_stake(conn, %{"address" => address}) do
    WalletClient.del_stakeout(address)
    redirect_to_page(conn, :stakes)
  end

  defp redirect_to_root(conn) do
    redirect_to_page(conn)
  end

  defp redirect_to_transactions(conn) do
    redirect_to_page(conn, :transactions)
  end

  defp redirect_to_settings(conn) do
    redirect_to_page(conn, :settings)
  end

  defp redirect_to_page(conn, :stakes) do
    redirect(conn, to: "/side_stakes")
  end

  defp redirect_to_page(conn, :settings) do
    redirect(conn, to: "/settings")
  end

  defp redirect_to_page(conn, :transactions) do
    redirect(conn, to: "/transactions")
  end

  defp redirect_to_page(conn) do
    redirect(conn, to: "/")
  end

  defp send_attachment(conn, {:file, path}, opts) do
    ext = Path.extname(path)
    filename = opts[:filename] || Path.basename(path)
    content_type = opts[:content_type] || case Path.extname(path) do
      "." <> ext -> Plug.MIME.type(ext)
      _ -> "application/octet-stream"
    end

    conn
    |> put_resp_content_type(content_type, opts[:charset])
    |> put_resp_header("content-disposition", ~s[attachment; filename="#{filename}"])
    |> send_file(conn.status || 200, path)
  end

  defp wallet_is_encrypted() do
    case WalletClient.wallet_info() do
      {:ok, %{"unlocked_until" => _}} ->
        true
      _ ->
        false
    end
  end
end
