defmodule PiWallet.Web.Api.TransactionController do
  use PiWallet.Web, :controller
  alias PiWallet.WalletClient

  def index(conn, %{"offset" => offset, "limit" => limit}) do
    {int_offset, _} = Integer.parse(offset)
    {int_limit, _} = Integer.parse(limit)
    {:ok, transactions} = WalletClient.transactions("", int_limit, int_offset)
    render(conn, "index.json", transactions: transactions, offset: offset, limit: limit)
  end
end