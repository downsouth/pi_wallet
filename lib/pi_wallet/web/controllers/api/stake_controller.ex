defmodule PiWallet.Web.Api.StakeController do
  use PiWallet.Web, :controller
  alias PiWallet.WalletClient

  def index(conn, _params) do
    {:ok, side_stakes} = WalletClient.list_stakeout
    IO.inspect side_stakes
    render(conn, "index.json", side_stakes: side_stakes)
  end
end