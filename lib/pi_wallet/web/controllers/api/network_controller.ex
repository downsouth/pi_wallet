defmodule PiWallet.Web.Api.NetworkController do
  use PiWallet.Web, :controller
  alias PiWallet.WifiScanner

  def index(conn, _params) do
    render(conn, "index.json", networks: WifiScanner.networks)
  end
end