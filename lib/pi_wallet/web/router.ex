defmodule PiWallet.Web.Router do
  use PiWallet.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    # plug BasicAuth, use_config: {:pi_wallet, :basic_auth}
    plug BasicAuth, callback: &PiWallet.BasicAuthCallback.check_credentials/3
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", PiWallet.Web do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index, as: :dashboard
    get "/transactions", TransactionController, :index
    get "/side_stakes", StakeController, :index
    get "/settings", SettingController, :index
    scope "/commands/" do
      post "/lock", CommandController, :lock
      post "/unlock", CommandController, :unlock
      post "/upgrade_interface", CommandController, :upgrade_interface
      post "/upgrade_daemon", CommandController, :upgrade_daemon
      post "/update_wifi_settings", CommandController, :update_wifi_settings
      post "/upload_wallet", CommandController, :upload_wallet
      get "/download_wallet", CommandController, :download_wallet
      post "/create_transaction", CommandController, :create_transaction
      post "/set_wallet_passphrase", CommandController, :set_wallet_passphrase
      post "/clear_transactions", CommandController, :clear_transactions
      post "/scan_for_transactions", CommandController, :scan_for_transactions
      post "/side_stakes", CommandController, :add_side_stake
      # delete "/side_stakes/:address", CommandController, :remove_side_stake
      # Should be using delete
      get "/side_stakes/:address", CommandController, :remove_side_stake
    end
  end

  # Other scopes may use custom stacks.
  scope "/api", PiWallet.Web.Api do
    pipe_through :api

    get "/wifi_networks", NetworkController, :index
    get "/transactions", TransactionController, :index
    get "/side_stakes", StakeController, :index
    get "/info", InfoController, :index
  end
end
