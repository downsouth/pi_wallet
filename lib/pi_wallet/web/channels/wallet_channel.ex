defmodule PiWallet.Web.WalletChannel do
  use Phoenix.Channel

  alias Phoenix.Socket.Broadcast

  def join(_, _payload, socket) do
    {:ok, socket}
  end

  def handle_info(%Broadcast{topic: _, event: ev, payload: payload}, socket) do
    push socket, ev, payload
    {:noreply, socket}
  end
end