defmodule PiWallet.Scheduler do
  use Quantum.Scheduler,
    otp_app: :pi_wallet
end